extends Camera

export var distance = 4.0
export var height = 2.0

# Called when the node enters the scene tree for the first time.
func _ready():
	set_physics_process(true)
	set_as_toplevel(true)

func _physics_process(delta):

	#target and position
	var t = get_parent().get_global_transform().origin
	var x = get_global_transform().origin

	#update offset
	var offset = x - t
	offset = distance * offset.normalized()
	offset.y = height
	x = t + offset
	look_at_from_position(x,t, Vector3(0,1,0))
