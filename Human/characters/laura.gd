extends KinematicBody

#exported parameters
export var speed = 5.0
export var gravity = Vector3(0,-9.81,0)

#local variables
var velocity = Vector3(0,0,0)
var default_rotation = 0

func _reay():
	default_rotation = $Laura/Laura.get_rotation().y

#movement
func get_movement():

	#keyboard
	var m = Vector2()
	if Input.is_action_pressed("ui_right"):
		m.x += 1
	if Input.is_action_pressed("ui_left"):
		m.x -= 1
	if Input.is_action_pressed("ui_up"):
		m.y += 1
	if Input.is_action_pressed("ui_down"):
		m.y -= 1
	
	#view transform
	var b = $View/Camera.global_transform.basis
	m = m.x * b.x - m.y * b.z
	m = m.normalized()
	return m


func _physics_process(delta):
	
	#update physics
	velocity += delta * gravity
	
	#move linearly
	var motion = velocity
	motion += speed * get_movement()
	motion = move_and_slide(motion)
	
	#update rotation
	if motion.length_squared() > 0.1:
		var angle = atan2(motion.x, motion.z) 
		var rotation = $Laura/Laura.get_rotation()
		rotation.y = angle - default_rotation
		$Laura/Laura.set_rotation(rotation)
	
	var amount = motion.length() / speed
	#amount = 2 * amount - 1
	amount = max(0,min(1, amount))
	#print("Speed", amount)
	$AnimationTree.set("parameters/speed/blend_amount", amount)
	
	
	##respawn if falling
	#if abs(velocity.y) > 99:
	#	get_tree().reload_current_scene()