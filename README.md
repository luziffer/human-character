# Human Character

example project setup of 3D game character with motion capture

![Herbie](sprites/herbie_playing.png)

## How to Use

- **Play:** Open the `Human/` project with Godot and start playing!
- **Add Human:** Create new people with MakeHuman and save them to `models/*.mhx2`.
- **Add Motion:** Add more motion capture files to `mocap/*.bvh`. 
  Check out Carnegie Mellon University motion capture dataset
- **Change:** Change details character model or animation using Blender and save to `scenes/`. 
  Automation scripts can be found in the scripts folder.
- **Export:** Save finalized models in glTF format to `Human/characters/*.bgl`.
  Now they are ready to be used in a game! 


## Tools Required

- ![Godot](https://godotengine.org/) (v3.0+) 
- ![Blender](https://www.blender.org/) (v2.8+) with plugins:
    - standard makhuman and biovision plugins 
    - ![BVH Retargeter](https://bitbucket.org/Diffeomorphic/retarget-bvh/src/master/)
- ![Makehuman](http://www.makehumancommunity.org/)
- Python ![BVH tools](https://github.com/20tab/bvh-python) library 


