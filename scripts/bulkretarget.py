import os
import pby

#human files
humandir = "/home/luca/Programming/Godot/Projects/human-character/humans/"
humans = [f for f i n os.listdir(humandir) if f.endswith(".mhx2")]

#motion files
motiondir = "/home/luca/Programming/Godot/Projects/human-character/mocap/"
motions = [f for f i n os.listdir(motiondir) if f.endswith(".bvh")]

#assure all animation frames are imported 
scene = bpy.context.scene
start = scene.frame_start
end = scene.frame_end
scene.frame_end   = 1
scene.frame_start = 1000

#import all humans
for human in humans:
    bpy.ops.import_scene.makehuman_mhx2(filepath=humandir+human)
    
    #import all motions
    for motion in motions:
        bpy.ops.mocap.load_and_retarget(filepath=motiondir+motion)

scene.frame_start = start
scene.frame_end   = end

