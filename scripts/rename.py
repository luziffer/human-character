import os
names = { #Copy, format to dictionary and paste from list
   "79_01" : "chopping wood",
    ... 
}
for nr in names:
    old_name = nr + ".bvh"
    new_name = nr + "_" + names[nr] + ".bvh"
    os.rename(old_name, new_name)
