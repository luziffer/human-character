import os
from bvh import Bvh
import pandas as pd

#list channel names
def all_channels(h):
    channels = []
    for joint in h.get_joints_names():
        for channel in h.joint_channels(joint):
            channels.append(joint + "_" + channel )
    return channels
    

#extract data in dataframe
def to_dataframe(h):
    
    #convert multi-line string to listed lists 
    lines = h.data.split("\n")
    start = 0
    for i in range( len(lines) ):
        if lines[i].startswith("MOTION"):
            start = i+3
    head = len( "\n".join(lines[0:start]) ) + 1 
    lines = lines[start:start+h.nframes]
    
    #convert string values to float
    data = []
    for i in range(h.nframes):
        line = lines[i]
        line = [ float(v) for v in line.split(" ")]
        data.append(line)
    
    return head, pd.DataFrame(data=data, columns=all_channels(h)) 

#convert dataframe back to string
def channel_string(df):
    return "\n".join(" ".join([str(v) for v in df.iloc[i]]) for i in range(len(df)) ) + "\n"


#subtract initial position
def recenter(fname):

    #extract file data
    with open(fname, "r") as f:
        h = Bvh(f.read())
    head, df = to_dataframe(h)
    head = h.data[0:head]
    
    #recenter to horizontal origin
    x = df["Hips_Xposition"]
    x = x - x[0]
    df["Hips_Xposition"] = x
    #y = y - y[0]
    #y = df["Hips_Yposition"]
    #df["Hips_Yposition"] = y
    z = df["Hips_Zposition"]
    z = z - z[0]
    df["Hips_Zposition"] = z
    channels = channel_string(df)
  
    #overwrite old file  
    with open(fname, "wt") as f:
        f.write(head + channels)
         
if __name__=="__main__":
    motiondir =  "../mocap/"
    for f in os.listdir(motiondir):
        if f.endswith(".bvh"):
            print('Recentering "%s"' % f)
            recenter(motiondir + f)


